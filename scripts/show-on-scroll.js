const scroll = window.requestAnimationFrame ||
            function(callback){ window.setTimeout(callback, 1000/60)};

const elementsToShow = document.querySelectorAll('.show-on-scroll');

const loop = () => {
  elementsToShow.forEach(function (element) {
    if (isElementInViewport(element)) {
      element.classList.add('is-visible');
    } else {
      element.classList.remove('is-visible');
    }
  });

  scroll(loop);
}

loop();


function isElementInViewport(el) {
  if (typeof jQuery === "function" && el instanceof jQuery) {
    el = el[0];
  }
  var rect = el.getBoundingClientRect();
  return (
    (rect.top <= 0
      && rect.bottom >= 0)
    ||
    (rect.bottom >= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.top <= (window.innerHeight || document.documentElement.clientHeight))
    ||
    (rect.top >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight))
  );
}

const animatedmask = document.getElementById("animation-mask");
let count = 0;

// Code for Chrome, Safari and Opera
animatedmask.addEventListener("webkitAnimationEnd", heroAnimationEndFunction);

// Standard syntax
animatedmask.addEventListener("animationend", heroAnimationEndFunction);

function heroAnimationEndFunction(e) {
  count++;
  let c = e.target;
  let countMask = animatedmask.querySelectorAll('.mask').length;
  if (count === countMask) {
    animatedmask.classList.add('completed');
  }
}
